This is a simple TouchDesigner network for controlling 3D generative scenes, saving short clips to disk, and rendering output via ArtNet/DMX.

In the lower left quadrant is a realtime preview of the generator, with controls on the left. 

-  The `Generators` list can be clicked to activate different generators. Currently two are available.
-  Four named sliders determine the behavior of the active generator.

`Record` begins a 30-second rendering process. The rendering is saved in two resolutions - 1024x1024 for projection, and 32x32 for Art-Net.

- Deactivating `Realtime` will ensure frames are not dropped while recording, at the expense of possible lag.
- The `1024`/`32` button toggles the resolution of the preview.

On the right half of the UI, a list of recorded clips is available. Pressing `Record` should add to the list when it is finished.

- Clips can be `Cue`d for output, and `Delete`d.
- The saved image series are available in the /movies subdirectory.

On the upper left, the output of the cued recording is displayed. Output is synchronized with a linear timecode input which can be accessed from the top level network. Currently, Art-Net and DMX are supported, in addition to a standard TOP stream. These will output at 32x32px. A total of 1024 pixels at 24 bits means that six universes are required to represent the full output stream. By default, universes 0-5 are used.

- Output methods can be toggled with `ArtNet` / `DMX`.
- Output can be toggled `Off` and `On`.
- `ArtNet Address` and `DMX Port` are necessary parameters for their respective output methods.
